import React from 'react'
import ReactDOM from 'react-dom'

import App from './container/App'

import { logger } from './helpers'

import config from './config/config'

if (!config.logging) {
    console.log = function (){}
}

logger(`This is the ${ENV_IS} build.`,"red")

ReactDOM.render(
        <App />,
	document.getElementById('root')
);