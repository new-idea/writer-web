const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const merge = require('webpack-merge')
const colors =require('colors')

module.exports = (env, argv) => {

    const config = {
        entry: {        
            main: './app/index'
        },
        output: {
            path: path.join(__dirname, 'dist'),
            filename: '[name].bundle.js',
            chunkFilename: '[name].bundle.js',
            publicPath: '/dist/',
        },
        devServer: {
            publicPath: '/dist/',
            port: 3000,
            hotOnly: true,
            historyApiFallback: true
        },
        plugins: [
            new webpack.HashedModuleIdsPlugin(),
            new HtmlWebpackPlugin({
                template: './app/index_template.html',
                filename: '../index.html'
            }),
            new webpack.DefinePlugin({
                ENV_IS: JSON.stringify(env),
                MODE_IS: JSON.stringify(argv.mode)
            })     
        ],
        optimization: {
            splitChunks: {
                chunks: 'all'
            }
        },
        module: {
            rules: [{
                test: /\.js$/, 
                include: path.join(__dirname, 'app'),
                use: {
                    loader: "babel-loader"
                }
            }]
        }
    };

    const message = "This is the "+env+" build."
    console.log(message.bold.red.bgWhite)

    if (argv.mode === 'development') {

        if (env == "development") {            
            return merge(config,{
                plugins: [
                    new webpack.NamedModulesPlugin(),
                    new webpack.HotModuleReplacementPlugin()
                ]
            })
        } 
        
        if (env == "staging") {
            return merge(config,{
                plugins: [
                    new webpack.NamedModulesPlugin(),
                    new webpack.HotModuleReplacementPlugin(),
                    
                ]
            })
        }       
    }
  
    if (argv.mode === 'production') {
        return merge(config,{
            output: {
                filename: '[name].bundle.[chunkhash].js',
                chunkFilename: '[name].bundle.[chunkhash].js'
            },
            plugins: [
                new CleanWebpackPlugin('dist')
            ]
        })
    }
};
