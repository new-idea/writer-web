let config
const baseConfig = {
    name: "New Idea Writer App",
    url: "standard.writer.nl",
    logging: true
}

if (ENV_IS == "development") {
    config = Object.assign(baseConfig, {
        url: "developent.writer.nl",
    })
}

if (ENV_IS == "staging") {
    config = Object.assign(baseConfig, {
        url: "staging.writer.nl"
    })
}

if (ENV_IS == "production") {
    config = Object.assign(baseConfig, {
        url: "production.writer.nl",
        logging: false
    })
}

export default config