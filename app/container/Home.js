import React from 'react'
import styled from 'styled-components'

import history from '../history'

const Wrapper = styled.div`
`

class Home extends React.Component {

    constructor(props) {
        super(props)

        this.navigate = this.navigate.bind(this)
    }

    navigate(route) {
        history.push(route)
    }

    render() {

        return (
            <Wrapper>
                <h1>Home!</h1>
                <span onClick={() => this.navigate("/test")}>TO TEST</span>
            </Wrapper>
        )
    }
}

export default Home