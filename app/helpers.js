
export const logger = (toLog,set_color,set_size) => {
    const color = set_color ? `color: ${set_color}` : "",
        size = set_size ? `font-size: ${set_size}px` : "",
        css = `${color}; ${size}`,
        string = `%c ${toLog}`
    console.log(string,css)
}