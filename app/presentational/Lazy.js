import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
    
    p {
        color: #C4E
    }
`

class Lazy extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <Wrapper>
                <p>LAZYBOY!!!!</p>
            </Wrapper>
        )
    }
}

export default Lazy