import React from 'react'
import styled from 'styled-components'
import Loadable from 'react-loadable';

import history from '../history'

const Loading = (props) => {
    if (props.pastDelay) {
        return <div>Loading...</div>;
    } else {
        return null
    }   
}

const Lazy = Loadable({
    loader: () => import(/* webpackChunkName: "lazy" */ '../presentational/Lazy'),
    loading: Loading,
    delay: 300
})

const Wrapper = styled.div`
`

class Test extends React.Component {

    constructor(props) {
        super(props)

        this.navigate = this.navigate.bind(this)
    }

    navigate(route) {
        history.push(route)
    }

    render() {

        return (
            <Wrapper>
                <h1>Test</h1>
                <span onClick={() => this.navigate("/")}>BACK</span>
                <Lazy/>
            </Wrapper>
        )
    }
}

export default Test