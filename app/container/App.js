import React from 'react'
import styled from 'styled-components'

import { hot } from 'react-hot-loader'

import { Router, Route, Switch } from 'react-router-dom'

import history from '../history'

import Home from './Home'
import Test from './Test'

const Wrapper = styled.div`
`

class App extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <Wrapper>
                <Router history={history}>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route path="/test" component={Test} />
                    </Switch>
                </Router> 
            </Wrapper>
        )
    }
}

export default hot(module)(App)